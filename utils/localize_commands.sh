#!/usr/bin/env bash
python ../setup.py extract_messages --input-dirs ../time2trip --output-file ../time2trip/locale/time2trip.pot -F ../babel.cfg

## for new deployment
#python ../setup.py init_catalog -l ru -i ../time2trip/locale/time2trip.pot -o ../time2trip/locale/ru/LC_MESSAGES/time2trip.po
#python ../setup.py init_catalog -l en -i ../time2trip/locale/time2trip.pot -o ../time2trip/locale/en/LC_MESSAGES/time2trip.po

python ../setup.py update_catalog -l ru -i ../time2trip/locale/time2trip.pot -o ../time2trip/locale/ru/LC_MESSAGES/time2trip.po
python ../setup.py update_catalog -l en -i ../time2trip/locale/time2trip.pot -o ../time2trip/locale/en/LC_MESSAGES/time2trip.po

#on centos use python2.7
python ../setup.py compile_catalog -d ../time2trip/locale -l ru --domain time2trip
python ../setup.py compile_catalog -d ../time2trip/locale -l en --domain time2trip


