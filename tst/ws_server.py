# -*- coding:utf-8 -*-

import gevent
from gevent.lock import Semaphore
from gevent.pywsgi import WSGIServer
from geventwebsocket.handler import WebSocketHandler
from datetime import datetime
from random import randint

def method (data):
    print('{} got data "{}"'.format(datetime.now().strftime('%H:%M:%S'), data))
    gevent.sleep(randint(1,10))
    print 'end'
    return 'finish'

def handle_ws (ws, sem, data):
    result = method(data)
    with sem:
        ws.send(result)

def app(environ, start_response):
    print 'start instance'
    ws = environ['wsgi.websocket']
    sem = Semaphore()
    while True:
        data = ws.receive()
        gevent.spawn(handle_ws, ws, sem, data)
    print 'end instance'


server = WSGIServer(('', 10004), app, handler_class=WebSocketHandler)
server.serve_forever()
