PATH_STATIC = './time2trip/static/'

module.exports = {
  // watch: true,
  entry: [
    PATH_STATIC + 'app/main.js'
  ],
  output: {
    path: PATH_STATIC + 'dist/',
    // publicPath: PATH_STATIC + 'dist',
    filename: 'build.js',
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015', 'stage-2'],
          plugins: ["transform-object-rest-spread"]
        }
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      }
    ]
  }
}