var
    gulp = require('gulp');
    coffee = require('gulp-coffee');
    stylus = require('gulp-stylus');
    sourcemaps = require('gulp-sourcemaps');
    notify = require('gulp-notify');
    concat = require('gulp-concat');
    debug = require('gulp-debug');
    plumber = require('gulp-plumber');
    coffeeConcat = require('gulp-coffeescript-concat');
    webpack = require('webpack-stream');

PATH_STATIC = './time2trip/static/'

var reportError = function (error) {
    notify({
            title: 'Gulp Task Error',
            message: 'Check the console.'
    }).write(error);
    console.log(error.toString());
    this.emit('end');
}

gulp.task('coffee', function() {
  return gulp.src(PATH_STATIC + 'coffee/*.coffee')
    .pipe(coffee({bare: true}))
    .pipe(gulp.dest(PATH_STATIC + 'js/'));
});

gulp.task('stylus', function () {
    return gulp.src([PATH_STATIC + 'css/*.styl'])
        .pipe(plumber({errorHandler: reportError}))
        .pipe(debug({title: 'stylus: '}))
        .pipe(sourcemaps.init())
        .pipe(stylus())
        .pipe(concat('styles.css'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(PATH_STATIC + 'css/'));
});

gulp.task('webpack', function(cb) {
   gulp.src(PATH_STATIC + 'app/main.js')
        .pipe(plumber({errorHandler: reportError}))
        .pipe(webpack( require('./webpack.config.js') ))
        .pipe(gulp.dest(PATH_STATIC + 'dist/'));
    cb();

});


gulp.task('done', ['coffee', 'stylus'], function () {
    return gulp.src('.')
        .pipe(notify('All tasks done'));
});

gulp.task('watch', [], function () {
    gulp.watch(PATH_STATIC + 'coffee/*.coffee', ['coffee', 'done']);
    gulp.watch(PATH_STATIC + 'css/*.styl', ['stylus', 'done']);
    gulp.watch([PATH_STATIC + 'app/**/*.vue', PATH_STATIC + 'app/**/*.js'], ['webpack', 'done']);
});



gulp.task('default', ['coffee', 'stylus', 'coffee', 'webpack',  'done', 'watch']);