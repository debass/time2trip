# -*- coding:utf-8 -*-

from pyramid.view import view_config
from pyramid.httpexceptions import HTTPFound
from pyramid.exceptions import NotFound


from time2trip.db import db_session, models


@view_config(route_name='route', renderer='../templates/route.mako', permission='user')
def route_page(request):
    route_id = request.matchdict['route_id']
    route = db_session.query(models.Route).filter(models.Route.id == route_id, models.Route.user_id == request.user.id).first()
    if not route:
        raise NotFound()

    return {'route': route}


@view_config(route_name='new_route', renderer='../templates/new_route.mako', permission='user')
def new_route_page(request):
    route_id = request.matchdict['route_id']
    route = db_session.query(models.Route).filter(models.Route.id == route_id, models.Route.user_id == request.user.id).first()
    if not route:
        raise NotFound()

    return {'route': route}


@view_config(route_name='route.delete', permission='user')
def delete(request):
    route_id = request.matchdict['route_id']
    route = db_session.query(models.Route).filter(models.Route.id == route_id, models.Route.user_id == request.user.id).first()
    if not route:
        raise NotFound()

    route.delete()
    request.session.flash(request.translate('Route was deleted!'))

    return HTTPFound(location=request.route_url('home'))