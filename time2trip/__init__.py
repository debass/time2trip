# -*- coding:utf-8 -*-

import os
import logging
import datetime


from pyramid.config import Configurator
from pyramid.security import Allow, ALL_PERMISSIONS, Everyone
from pyramid.authentication import SessionAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
from pyramid.session import SignedCookieSessionFactory
from pyramid.events import NewResponse, NewRequest, BeforeRender

from sqlalchemy import engine_from_config
from pyramid import renderers

from time2trip.db import init_db
# from easytrip.utils.sendgrid_mailer import SendGridMailer
# from easytrip.utils.telegram import Telegram

def main(global_config, **settings):
    engine = engine_from_config(settings, 'sqlalchemy.')

    init_db(engine)

    import time2trip.helpers

    config = Configurator(settings=settings,
                          root_factory=RootFactory)

    authn_policy = SessionAuthenticationPolicy(
        callback=time2trip.helpers.get_identifiers
    )

    authz_policy = ACLAuthorizationPolicy()

    session_factory = SignedCookieSessionFactory(
        secret='sosecret',
        hashalg='sha512',
        timeout=60 * 60 * 24 * 3,
        max_age=60 * 60 * 24 * 3,
        # domain=settings.get('cookie_domain', 'time2trip.in'),
        set_on_exception=True

    )

    config.set_authentication_policy(authn_policy)
    config.set_authorization_policy(authz_policy)
    config.set_session_factory(session_factory)

    config.set_request_property(time2trip.helpers.get_user, 'user')

    # config.registry['mailer'] = SendGridMailer(
    #     api_key=settings.get('sendgrid.key'),
    #     sender=settings.get('sendgrid.sender')
    # )
    # config.registry['telegram'] = Telegram(token=settings.get('telegram.key'))

    json_renderer = renderers.JSON()
    json_renderer.add_adapter(datetime.date, time2trip.helpers.datetime_json_encoder)
    json_renderer.add_adapter(datetime.datetime, time2trip.helpers.datetime_json_encoder)
    config.add_renderer('json', json_renderer)

    config.add_static_view(name='static', path='time2trip:static/', cache_max_age=3600)
    config.set_locale_negotiator(time2trip.helpers.locale_negotiator)

    config.add_route('home', '/')
    config.add_route('login', '/login')
    config.add_route('logout', '/logout')
    config.add_route('registration', '/registration')
    config.add_route('locale', '/locale')

    config.add_route('route', '/route/{route_id}')
    config.add_route('new_route', '/new_route/{route_id}')
    config.add_route('route.delete', '/route/{route_id}/delete')


    import time2trip.views
    import time2trip.subscribers

    config.scan(time2trip.views)
    config.scan(time2trip.subscribers)

    config.add_translation_dirs('time2trip:locale')

    return config.make_wsgi_app()


class RootFactory(object):
    __acl__ = [
        (Allow, Everyone, 'view'),
        (Allow, 'group:user', 'user'),
        (Allow, 'group:admin', ALL_PERMISSIONS)
    ]

    def __init__(self, request):
        pass