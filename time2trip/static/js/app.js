var AddPoint, AppMenu, AppPageLayout, EditMixin, EditRoute, MapMixin, Point, PointView, Subpoint, SubpointView, SubpointWizard__Custom, SubpointWizard__Foursquare, SubpointWizzard;

AppMenu = {
  template: "<div class=\"app-menu\">\n  <ul>\n    <li>\n        <router-link :to=\"{name: 'edit_route'}\" class=\"btn-default btn menu-btn\"><i class=\"fa fa-pencil\"></i></router-link>\n    </li>\n    <li><a href=\"#\" class=\"btn-default btn menu-btn\"><i class=\"fa fa-users\"></a></li>\n    <li><a href=\"#\" class=\"btn-default btn menu-btn\"><i class=\"fa fa-bars\"></i></a></li>\n    <li><a href=\"#\" class=\"btn-default btn menu-btn\">Add</a></li>\n  </ul>\n</div>"
};

AppPageLayout = {
  props: ['class_name', 'title'],
  template: "<div :class=\"class_name\">\n    <div class=\"header\">\n        <div class=\"header-btn\">\n            <button @click=\"go_back\" class=\"btn btn-default btn-lg\"><span class=\"glyphicon glyphicon-remove\"></span></a>\n        </div>\n        <div class=\"header-title\">\n            {{title}}\n        </div>\n    </div>\n    <slot></slot>\n</div>",
  methods: {
    go_back: function() {
      return this.$router.go(-1);
    }
  }
};

MapMixin = {
  methods: {
    fit_route: function() {
      return console.log('fit_route');
    },
    fit_route: function(point) {
      return console.log('fit_route');
    },
    add_point_marker: function(point) {
      return console.log('add_point_marker');
    }
  }
};

EditMixin = {};

EditRoute = {
  components: {
    'app-page-layout': AppPageLayout
  },
  template: "<app-page-layout :class_name = 'class_name' :title = 'title'>\n    <input type=\"text\" v-model='route.title' v-validate data-vv-rules=\"required|min:4|max:10\" :class=\"{'has-error': errors.has('title') }\" name=\"title\" class=\"form-control\">\n    <span v-show=\"errors.has('title')\" class=\"help is-danger\">{{ errors.first('title') }}</span>\n    <textarea name=\"description\" v-validate data-vv-rules=\"max:100\" :class=\"{'has-error': errors.has('description') }\" class=\"form-control\" rows=\"3\" style=\"resize:none;\" v-model='route.description'></textarea>\n    <span v-show=\"errors.has('description')\" class=\"help is-danger\">{{ errors.first('description') }}</span>\n    <button class=\"btn btn-default btn-block\" @click=\"submit\" :disabled=\"errors.errors.length > 0\" >Save</button>\n</app-page-layout>",
  data: function() {
    return {
      title: 'route edit',
      class_name: 'edit-route',
      route: {}
    };
  },
  beforeMount: function() {
    return this.route = _.assignIn(this.route, this.$store.getters.route_for_edit);
  },
  methods: {
    submit: function() {
      if (!this.errors.errors.length) {
        return this.$store.dispatch('edit_route', this.route).then(this.$router.go(-1));
      }
    }
  }
};

AddPoint = {
  template: "<div class=\"input-group add-point-ctrl\">\n  <input type=\"text\" class=\"form-control\" v-model=\"text\" id=\"tbx_search_point\" @input=\"drop_place\" @keyup.enter=\"add_point\" placeholder=\"City\">\n  <span class=\"input-group-btn\">\n    <button type=\"submit\" class=\"btn-default btn\" :disabled=\"place == null\" @click=\"add_point\">Add</button>\n  </span>\n</div>",
  data: function() {
    return {
      place: null,
      text: ''
    };
  },
  mounted: function() {
    var _this, autoComplete;
    autoComplete = new google.maps.places.Autocomplete(document.getElementById('tbx_search_point'), {
      types: ['(cities)']
    });
    autoComplete.bindTo('bounds', window['map']);
    _this = this;
    return google.maps.event.addListener(autoComplete, 'place_changed', (function(_this) {
      return function() {
        _this.place = autoComplete.getPlace();
        _this.text = _this.place.formatted_address;
        if (_this.place.geometry.viewport) {
          return window.map.fitBounds(_this.place.geometry.viewport);
        } else {
          window.map.setCenter(_this.place.geometry.location);
          return window.map.setZoom(10);
        }
      };
    })(this));
  },
  methods: {
    drop_place: function() {
      return this.place = null;
    },
    add_point: function() {
      var jsonData;
      if (!this.place) {
        return;
      }
      jsonData = {
        'route_id': this.$store.state.route.id,
        'lat': this.place.geometry.location.lat(),
        'lng': this.place.geometry.location.lng(),
        'title': this.place.address_components[0].long_name,
        'address': this.place.formatted_address,
        'address_components': this.place.address_components,
        'gmaps_placeid': this.place.place_id
      };
      if (this.place.geometry.viewport) {
        jsonData.bounds = {
          'southwest': coords_helper(this.place.geometry.viewport.getSouthWest().toString()),
          'northeast': coords_helper(this.place.geometry.viewport.getNorthEast().toString())
        };
      }
      return this.$store.dispatch('add_point', jsonData).then((function(_this) {
        return function(point_id) {
          _this.drop_place();
          return _this.text = '';
        };
      })(this));
    }
  }
};

Point = {
  props: ['point'],
  template: "<li class=\"list-group-item point\" :class=\"{open: point.id == current_point.id}\" @click=\"choose_point(point.id)\">\n<div class=\"sort-handler\"><i class=\"fa fa-bars\"></i></div>\n    <div class=\"point-name\"> {{title}} </div>\n<ul class=\"list-group-submenu\">\n  <li class=\"list-group-submenu-item\">\n    <button class=\"btn btn-danger btn-sm\" @click=\"delete_point(point.id)\">\n      <span class=\"glyphicon glyphicon-remove\"></span>\n    </button>\n  </li>\n\n  <!-- <li class=\"list-group-submenu-item\">\n    <router-link class=\"btn btn-default btn-sm\" :to=\"{name: 'point.edit', params: {'point_id': point.id}}\">\n      <span class=\"glyphicon glyphicon-pencil\"></span>\n    </router-link>\n  </li> -->\n\n  <li class=\"list-group-submenu-item note-btn\">\n    <button class=\"btn btn-default btn-sm\">\n      <span class=\"fa fa-sticky-note-o\"></span>\n    </button>\n  </li>\n\n  <li class=\"list-group-submenu-item subpoints-btn\">\n    <router-link class=\"btn btn-default btn-sm\" :to=\"{name: 'subpoints', params: {point_id: point.id }}\">\n      <span class=\"glyphicon glyphicon-map-marker\"></span>\n    </router-link>\n  </li>\n</ul>\n</li>",
  computed: {
    current_point: function() {
      return this.$store.state.current_point;
    },
    title: function() {
      return this.point.locale.title[this.$store.state.user.locale];
    }
  },
  methods: {
    choose_point: function(point_id) {
      return this.$store.commit('choose_point', point_id);
    },
    delete_point: function(point_id) {
      return this.$store.dispatch('delete_point', point_id);
    }
  }
};

PointView = {
  components: {
    'my-point': Point,
    'add-point': AddPoint
  },
  template: "<div class=\"points\">\n  <add-point></add-point>\n  <ul class=\"list-group-custom\">\n      <my-point v-for=\"point in points\" :point=\"point\" ></my-point>\n  </ul>\n</div>",
  computed: {
    points: function() {
      return this.$store.state.route.points;
    }
  }
};

Subpoint = {
  props: ['subpoint'],
  template: "<li class=\"list-group-item\">\n    <div class=\"subpoint-name\"> {{ subpoint.title }}</div>\n    <ul class=\"list-group-submenu\">\n        <li class=\"list-group-submenu-item\">\n        <button class=\"btn btn-danger btn-sm\">\n            <span class=\"glyphicon glyphicon-remove\"></span>\n        </button>\n        </li>\n\n        <li class=\"list-group-submenu-item \">\n            <button class=\"btn btn-default btn-sm\">\n                <span class=\"glyphicon glyphicon-pencil\"></span>\n            </button>\n        </li>\n    </ul>\n</li>"
};

SubpointView = {
  components: {
    'app-page-layout': AppPageLayout,
    'my-subpoint': Subpoint
  },
  template: "<app-page-layout :class_name = 'class_name' :title = 'title'>\n    <router-link class=\"btn btn-primary btn-block\" :to=\"{name: 'subpoints.add', params: {point_id: current_point.id }}\">Add </router-link>\n    <ul class=\"list-group-custom\">\n        <my-subpoint v-for=\"subpoint in current_point.subpoints\" :subpoint=\"subpoint\"></my-subpoint> \n    </ul>\n</app-page-layout>",
  data: function() {
    return {
      class_name: 'subpoints'
    };
  },
  computed: {
    current_point: function() {
      return this.$store.getters.current_point;
    },
    title: function() {
      return this.current_point.locale.title.en;
    }
  },
  created: function() {
    if (this.current_point.id !== this.$route.params.point_id) {
      return this.$store.commit('choose_point', parseInt(this.$route.params.point_id));
    }
  },
  watch: {
    '$route': function() {
      return this.$store.commit('choose_point', parseInt(this.$route.params.point_id));
    }
  }
};

SubpointWizard__Custom = {
  template: "<div>\n    <div class=\"input-group\" >\n        <input type=\"text\" class=\"form-control input-sm\" v-model=\"search_box\" v-validate data-vv-rules=\"required|min:4\" :class=\"{'has-error': errors.has('search-box') }\" name=\"search-box\" placeholder=\"Address or coordinates\">\n        <span class=\"input-group-btn\">\n            <button class=\"btn-default btn btn-sm\" @click=\"process\"><span class=\"glyphicon glyphicon-search\"></span> Search</button>\n        </span>\n    </div>\n    <div class=\"custom-note\">\n        Click to the any place on map. We try to get closest address to choosed point\n    </div>\n</div>",
  data: function() {
    return {
      search_box: ''
    };
  },
  methods: {
    process: function() {
      return console.log(this.search_box);
    }
  }
};

SubpointWizard__Foursquare = {
  template: "<div>\n    foursquare\n</div>"
};

SubpointWizzard = {
  components: {
    'app-page-layout': AppPageLayout,
    'custom': SubpointWizard__Custom,
    'foursquare': SubpointWizard__Foursquare
  },
  template: "<app-page-layout :class_name = 'class_name' :title = 'title'>\n    <div class=\"wizard-sources\">\n        <div class='input-group-btn' data-toggle=\"buttons\">\n            <label class=\"btn btn-default\" :class=\"{'active': wizzard_source == 'foursquare'}\" @click=\"set_wizzard('foursquare')\">\n                <input name=\"s\" type=\"radio\" value=\"foursquare\" v-model=\"wizzard_source\"  > Foursquare\n            </label>\n            <label class=\"btn btn-default\" :class=\"{'active': wizzard_source == 'custom'}\" @click=\"set_wizzard('custom')\">\n                <input name=\"s\" type=\"radio\" value=\"custom\" v-model=\"wizzard_source\"> Custom\n            </label>\n        </div>\n    </div>\n    <component v-bind:is=\"wizzard_source\"></component>\n</app-page-layout>",
  data: function() {
    return {
      class_name: 'subpoint-wizzard',
      title: 'add subpoint',
      wizzard_source: 'foursquare'
    };
  },
  methods: {
    set_wizzard: function(t) {
      return this.wizzard_source = t;
    }
  }
};

window.loadingMapDefered = Q.defer();

window.loadingMapPromise = loadingMapDefered.promise;

window.googleMapInit = function() {
  console.log('loaded');
  return window.loadingMapDefered.resolve();
};

window.store = new Vuex.Store({
  state: {
    route: {},
    route_loaded: false,
    screen_spinner_active: false,
    app_spinner_active: false,
    refbooks: {},
    user: {},
    current_point: {},
    current_subpoint: {},
    map_markers: []
  },
  mutations: {
    set: function(state, data) {
      return _.assignIn(state, data);
    },
    choose_point: function(state, point_id) {
      var point;
      point = state.route.points.filter(function(p) {
        return p.id === point_id;
      });
      if (point) {
        return state.current_point = point[0];
      }
    },
    edit_route: function(state, route_obj) {
      return _.assignIn(state.route, route_obj);
    },
    add_point: function(state, point_obj) {
      return state.route.points.push(point_obj);
    },
    delete_point: function(state, point_id, external) {
      var point;
      if (external == null) {
        external = false;
      }
      point = state.route.points.filter(function(p) {
        return p.id === point_id;
      });
      if (point) {
        point = point[0];
        if (point.id === state.current_point.id) {
          state.current_point = {};
        }
        return state.route.points.splice(state.route.points.indexOf(point), 1);
      }
    }
  },
  actions: {
    edit_route: function(context, route_data) {
      var d;
      d = Q.defer();
      window.ws.request('edit_route', route_data).then(function(response) {
        if (response.success) {
          context.commit('edit_route', response.result.route);
        }
        return d.resolve(response.result.route);
      }).fail(function(err) {
        return console.log(err);
      });
      return d.promise;
    },
    add_point: function(context, point_data) {
      var d;
      d = Q.defer();
      window.ws.request('add_point', point_data).then(function(response) {
        if (response.success) {
          context.commit('add_point', response.result.point);
          return d.resolve(response.result.point.id);
        }
      }).fail(function(err) {
        return console.log(err);
      });
      return d.promise;
    },
    delete_point: function(context, point_id) {
      var d;
      d = Q.defer();
      window.ws.request('delete_point', {
        'id': point_id
      }).then(function(response) {
        if (response.success) {
          context.commit('delete_point', response.result.point.id);
          return d.resolve(response.result.point);
        }
      }).fail(function(err) {
        return console.log(err);
      });
      return d.promise;
    }
  },
  getters: {
    current_point: function(state) {
      return state.current_point;
    },
    route_for_edit: function(state) {
      return {
        'title': state.route.title,
        'description': state.route.description
      };
    }
  }
});

window.router = new VueRouter({
  routes: [
    {
      path: '/',
      component: PointView,
      name: 'points'
    }, {
      path: '/edit',
      component: EditRoute,
      name: 'edit_route'
    }, {
      path: '/point/:point_id/subpoints',
      component: SubpointView,
      name: 'subpoints'
    }, {
      path: '/point/:point_id/subpoints/add',
      component: SubpointWizzard,
      name: 'subpoints.add'
    }
  ]
});

router.beforeEach(function(to, from, next) {
  var wait_for_route;
  console.log(to);
  wait_for_route = function() {
    if (window.store.state.route_loaded) {
      return next();
    } else {
      return setTimeout(wait_for_route, 10);
    }
  };
  return wait_for_route();
});

Vue.use(VeeValidate);

window.app = new Vue({
  el: '#app',
  store: store,
  router: router,
  components: {
    'app-menu': AppMenu,
    'point-list': PointView
  },
  data: {
    ws: null,
    current_view: 'point-list'
  },
  created: function() {
    window.ws = new window.WSClient(window.location.hostname + ":9090", window.route_id, window.token);
    return this.init_route();
  },
  methods: {
    init_route: function() {
      return window.ws.request('get_route', {
        id: window.route_id
      }).then((function(_this) {
        return function(response) {
          var map_markers, map_options;
          if (response.success) {
            _this.$store.commit('set', {
              user: response.result.user,
              route: response.result.route,
              route_loaded: true
            });
            map_options = {
              streetViewControl: false,
              mapTypeControl: true,
              mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
                position: google.maps.ControlPosition.LEFT_TOP
              },
              styles: [
                {
                  featureType: 'poi',
                  elementType: 'label',
                  stylers: [
                    {
                      visibility: 'off'
                    }
                  ]
                }
              ]
            };
            window.map = new google.maps.Map(document.getElementById("map_canvas"), map_options);
            map.setCenter(new google.maps.LatLng(-25.363, 131.044));
            map.setZoom(2);
            window.map_loaded_defer.resolve();
            map_markers = response.result.route.points.map(function(point) {
              return new google.maps.Marker({
                position: new google.maps.LatLng(point.lat, point.lng),
                title: '123'
              });
            });
            return _this.$store.commit('set', {
              map_markers: map_markers
            });
          } else {
            return show_notify(response.err_msg, 'topCenter', 'error');
          }
        };
      })(this));
    }
  },
  computed: {
    screen_spinner_active: function() {
      if (this.$store.state.route_loaded === false || this.$store.state.screen_spinner_active === true) {
        return true;
      }
      return false;
    }
  }
});
