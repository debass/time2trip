window.show_notify = function(text, position, type) {
  if (position == null) {
    position = 'topLeft';
  }
  if (type == null) {
    type = 'information';
  }
  return noty({
    text: text,
    type: type,
    layout: position,
    theme: 'custom-notify',
    timeout: 3000,
    dismissQueue: true,
    maxVisible: 3,
    animation: {
      open: 'animated bounceInLeft',
      close: 'animated bounceOutLeft',
      easing: 'swing',
      speed: 500
    }
  });
};

window.show_confirm = function(text, position, modal, ok_callback, ok_callback_params, cancel_callback, cancel_callback_params) {
  if (position == null) {
    position = 'center';
  }
  if (modal == null) {
    modal = false;
  }
  if (ok_callback_params == null) {
    ok_callback_params = [];
  }
  if (cancel_callback == null) {
    cancel_callback = null;
  }
  if (cancel_callback_params == null) {
    cancel_callback_params = [];
  }
  return noty({
    text: text,
    layout: position,
    modal: modal,
    theme: 'custom-notify',
    buttons: [
      {
        addClass: 'btn btn-primary btn-sm',
        text: 'Ok',
        onClick: function($noty) {
          $noty.close();
          return ok_callback.apply(window, ok_callback_params);
        }
      }, {
        addClass: 'btn btn-default btn-sm',
        text: 'Cancel',
        onClick: function($noty) {
          $noty.close();
          if (cancel_callback) {
            if (cancel_callback_params != null ? cancel_callback_params.length : void 0) {
              return cancel_callback.apply(window, cancel_callback_params);
            } else {
              return cancel_callback();
            }
          }
        }
      }
    ]
  });
};

window.coords_helper = function(coord) {
  var tmp, x;
  tmp = coord.slice(1, -1).replace(' ', '').split(',');
  return (function() {
    var i, len, results;
    results = [];
    for (i = 0, len = tmp.length; i < len; i++) {
      x = tmp[i];
      results.push(parseFloat(x));
    }
    return results;
  })();
};
