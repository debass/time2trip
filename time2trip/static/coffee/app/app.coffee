window.router = new VueRouter({
    routes: [
        {path: '/', component: PointView, name: 'points'}
        {path: '/edit', component: EditRoute, name: 'edit_route'}
        {path: '/point/:point_id/subpoints', component: SubpointView, name: 'subpoints'}
        {path: '/point/:point_id/subpoints/add', component: SubpointWizzard, name: 'subpoints.add'}
    ]
})

router.beforeEach((to, from, next) ->
    console.log to
    wait_for_route = () ->
        if window.store.state.route_loaded
            # if point
            next()
        else
            setTimeout(wait_for_route, 10)
    wait_for_route()
)


Vue.use(VeeValidate)


window.app = new Vue ({
    el: '#app',
    store,
    router,
    components: {
        'app-menu': AppMenu,
        'point-list': PointView,

        # 'subpoint-list': SubpointList
    }
    data: {
        ws: null
        current_view: 'point-list'
    },
    created: () ->
        window.ws = new window.WSClient("#{window.location.hostname}:9090", window.route_id, window.token)
        @init_route()

    methods: {
        init_route: () ->
            window.ws.request('get_route', {id:  window.route_id}).then(
                (response) =>
                    if response.success
                        @$store.commit('set', {
                            user: response.result.user,
                            route: response.result.route,
                            route_loaded: true
                        })
                        map_options = {
                            streetViewControl: false,
                            mapTypeControl: true,
                            mapTypeControlOptions: {
                                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
                                position: google.maps.ControlPosition.LEFT_TOP,
                            },
                            styles: [
                                {
                                    featureType: 'poi',
                                    elementType: 'label',
                                    stylers: [
                                        {visibility: 'off'}
                                    ]
                                }
                            ],
                        }
                        window.map = new google.maps.Map(document.getElementById("map_canvas"), map_options)
                        map.setCenter(new google.maps.LatLng(-25.363, 131.044))
                        map.setZoom(2)
                        window.map_loaded_defer.resolve()
                        map_markers = response.result.route.points.map(
                            (point) ->
                                new google.maps.Marker({
                                    position: new google.maps.LatLng(point.lat, point.lng),
                                    title: '123'
                                })
                        )
                        @$store.commit('set', { map_markers: map_markers })


                    else
                        show_notify(response.err_msg, 'topCenter', 'error')
            )

    }
    computed: {
        screen_spinner_active: () ->
            if @$store.state.route_loaded == false or @$store.state.screen_spinner_active == true
                return true
            return false

        # route_loaded: () ->
            # return @$store.state.route_loaded

        # point_count: () ->
        #     return @$store.getters.point_count

        # points: () ->
        #     return @$store.state.route.points

        # current_point: () ->
        #     @$store.getters.current_point


    }
})



