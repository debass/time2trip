AppMenu = {
  template: """
    <div class="app-menu">
      <ul>
        <li>
            <router-link :to="{name: 'edit_route'}" class="btn-default btn menu-btn"><i class="fa fa-pencil"></i></router-link>
        </li>
        <li><a href="#" class="btn-default btn menu-btn"><i class="fa fa-users"></a></li>
        <li><a href="#" class="btn-default btn menu-btn"><i class="fa fa-bars"></i></a></li>
        <li><a href="#" class="btn-default btn menu-btn">Add</a></li>
      </ul>
    </div>
  """
}


AppPageLayout = {
    props: ['class_name', 'title']
    template: """
        <div :class="class_name">
            <div class="header">
                <div class="header-btn">
                    <button @click="go_back" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <div class="header-title">
                    {{title}}
                </div>
            </div>
            <slot></slot>
        </div>
    """
    methods: {
        go_back: () ->
            @$router.go(-1)
    }
}

MapMixin = {
    methods: {
        fit_route: () ->
            console.log 'fit_route'

        fit_route: (point) ->
            console.log 'fit_route'

        add_point_marker: (point) ->
            console.log 'add_point_marker'

    }
}

############
EditMixin = {}


EditRoute = {
    components: {'app-page-layout': AppPageLayout}
    template: """
        <app-page-layout :class_name = 'class_name' :title = 'title'>
            <input type="text" v-model='route.title' v-validate data-vv-rules="required|min:4|max:10" :class="{'has-error': errors.has('title') }" name="title" class="form-control">
            <span v-show="errors.has('title')" class="help is-danger">{{ errors.first('title') }}</span>
            <textarea name="description" v-validate data-vv-rules="max:100" :class="{'has-error': errors.has('description') }" class="form-control" rows="3" style="resize:none;" v-model='route.description'></textarea>
            <span v-show="errors.has('description')" class="help is-danger">{{ errors.first('description') }}</span>
            <button class="btn btn-default btn-block" @click="submit" :disabled="errors.errors.length > 0" >Save</button>
        </app-page-layout>
    """
    data: () ->
        title: 'route edit'
        class_name: 'edit-route'
        route: {}

    beforeMount: () ->
        @route = _.assignIn(@route,  @$store.getters.route_for_edit)

    methods: {
        submit: () ->
            if not @errors.errors.length
                @$store.dispatch('edit_route', @route).then(@$router.go(-1))
    }

}


############

AddPoint = {
    template: """
    <div class="input-group add-point-ctrl">
      <input type="text" class="form-control" v-model="text" id="tbx_search_point" @input="drop_place" @keyup.enter="add_point" placeholder="City">
      <span class="input-group-btn">
        <button type="submit" class="btn-default btn" :disabled="place == null" @click="add_point">Add</button>
      </span>
    </div>
    """
    data: () ->
        return {
            place: null
            text: ''
        }

    mounted: () ->
        autoComplete = new google.maps.places.Autocomplete(document.getElementById('tbx_search_point'), {types: ['(cities)']})
        autoComplete.bindTo('bounds', window['map'])

        _this = @

        google.maps.event.addListener(autoComplete, 'place_changed', () =>
            @place = autoComplete.getPlace()
            @text = @place.formatted_address
            if @place.geometry.viewport
                window.map.fitBounds(@place.geometry.viewport)
            else
                window.map.setCenter(@place.geometry.location)
                window.map.setZoom(10)
        )

    methods: {
        drop_place: () ->
            @place = null

        add_point: () ->
            if not @place
                return

            jsonData = {
                'route_id':@$store.state.route.id
                'lat': @place.geometry.location.lat()
                'lng': @place.geometry.location.lng()
                'title': @place.address_components[0].long_name
                'address': @place.formatted_address
                'address_components': @place.address_components
                'gmaps_placeid': @place.place_id,
            }

            if @place.geometry.viewport
                jsonData.bounds = {
                    'southwest': coords_helper(@place.geometry.viewport.getSouthWest().toString()),
                    'northeast':coords_helper(@place.geometry.viewport.getNorthEast().toString())
                }
            @$store.dispatch('add_point', jsonData).then(
                (point_id) =>
                    @drop_place()
                    @text = ''

            )
    }
}

Point = {
    props: ['point'],
    template: """<li class="list-group-item point" :class="{open: point.id == current_point.id}" @click="choose_point(point.id)">
  <div class="sort-handler"><i class="fa fa-bars"></i></div>
      <div class="point-name"> {{title}} </div>
  <ul class="list-group-submenu">
    <li class="list-group-submenu-item">
      <button class="btn btn-danger btn-sm" @click="delete_point(point.id)">
        <span class="glyphicon glyphicon-remove"></span>
      </button>
    </li>

    <!-- <li class="list-group-submenu-item">
      <router-link class="btn btn-default btn-sm" :to="{name: 'point.edit', params: {'point_id': point.id}}">
        <span class="glyphicon glyphicon-pencil"></span>
      </router-link>
    </li> -->

    <li class="list-group-submenu-item note-btn">
      <button class="btn btn-default btn-sm">
        <span class="fa fa-sticky-note-o"></span>
      </button>
    </li>

    <li class="list-group-submenu-item subpoints-btn">
      <router-link class="btn btn-default btn-sm" :to="{name: 'subpoints', params: {point_id: point.id }}">
        <span class="glyphicon glyphicon-map-marker"></span>
      </router-link>
    </li>
  </ul>
</li>"""

    computed: {
        current_point: () ->
            @$store.state.current_point

        title: () ->
            @point.locale.title[@$store.state.user.locale]
    }

    methods: {
        choose_point: (point_id) ->
            @$store.commit('choose_point', point_id)

        delete_point: (point_id) ->
            @$store.dispatch('delete_point', point_id)
    }

}


PointView = {
    components: {'my-point': Point, 'add-point': AddPoint}
    template : """
        <div class="points">
          <add-point></add-point>
          <ul class="list-group-custom">
              <my-point v-for="point in points" :point="point" ></my-point>
          </ul>
        </div>
    """
    computed: {
        points: () ->
            return @$store.state.route.points
    }
}
###########

Subpoint = {
    props: ['subpoint']
    template: """
    <li class="list-group-item">
        <div class="subpoint-name"> {{ subpoint.title }}</div>
        <ul class="list-group-submenu">
            <li class="list-group-submenu-item">
            <button class="btn btn-danger btn-sm">
                <span class="glyphicon glyphicon-remove"></span>
            </button>
            </li>

            <li class="list-group-submenu-item ">
                <button class="btn btn-default btn-sm">
                    <span class="glyphicon glyphicon-pencil"></span>
                </button>
            </li>
        </ul>
    </li>
    """
}

SubpointView = {
    components: {
        'app-page-layout': AppPageLayout
        'my-subpoint': Subpoint
    }
    template: """
        <app-page-layout :class_name = 'class_name' :title = 'title'>
            <router-link class="btn btn-primary btn-block" :to="{name: 'subpoints.add', params: {point_id: current_point.id }}">Add </router-link>
            <ul class="list-group-custom">
                <my-subpoint v-for="subpoint in current_point.subpoints" :subpoint="subpoint"></my-subpoint> 
            </ul>
        </app-page-layout>
    """
    data: () ->
        class_name: 'subpoints'

    computed: {
        current_point: () ->
            return @$store.getters.current_point

        title: () ->
            @current_point.locale.title.en
    }
    created: () ->
        if @current_point.id != @$route.params.point_id
            @$store.commit('choose_point', parseInt(@$route.params.point_id))
    
    watch: {
        '$route': () ->
            @$store.commit('choose_point', parseInt(@$route.params.point_id))
    }
}

SubpointWizard__Custom = {
    template: """
        <div>
            <div class="input-group" >
                <input type="text" class="form-control input-sm" v-model="search_box" v-validate data-vv-rules="required|min:4" :class="{'has-error': errors.has('search-box') }" name="search-box" placeholder="Address or coordinates">
                <span class="input-group-btn">
                    <button class="btn-default btn btn-sm" @click="process"><span class="glyphicon glyphicon-search"></span> Search</button>
                </span>
            </div>
            <div class="custom-note">
                Click to the any place on map. We try to get closest address to choosed point
            </div>
        </div>
    """

    data: () ->
        search_box: ''

    methods: {
        process: () ->
            console.log @search_box
    }

}


SubpointWizard__Foursquare = {
    template: """
        <div>
            foursquare
        </div>
    """
}


SubpointWizzard = {
    components: {
        'app-page-layout': AppPageLayout,
        'custom': SubpointWizard__Custom,
        'foursquare': SubpointWizard__Foursquare
    }
    template: """
        <app-page-layout :class_name = 'class_name' :title = 'title'>
            <div class="wizard-sources">
                <div class='input-group-btn' data-toggle="buttons">
                    <label class="btn btn-default" :class="{'active': wizzard_source == 'foursquare'}" @click="set_wizzard('foursquare')">
                        <input name="s" type="radio" value="foursquare" v-model="wizzard_source"  > Foursquare
                    </label>
                    <label class="btn btn-default" :class="{'active': wizzard_source == 'custom'}" @click="set_wizzard('custom')">
                        <input name="s" type="radio" value="custom" v-model="wizzard_source"> Custom
                    </label>
                </div>
            </div>
            <component v-bind:is="wizzard_source"></component>
        </app-page-layout>
    """
    data: () ->
        class_name: 'subpoint-wizzard'
        title: 'add subpoint'
        wizzard_source: 'foursquare'

    methods: {
        set_wizzard: (t) ->
            @wizzard_source = t
    }

}


