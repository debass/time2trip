window.show_notify = (text, position='topLeft', type='information') ->
    noty {
        text: text,
        type: type,
        layout: position,
        theme: 'custom-notify',
        timeout: 3000,
        dismissQueue: true,
        maxVisible: 3,
        animation: {
            open  : 'animated bounceInLeft',
            close : 'animated bounceOutLeft',
            easing: 'swing',
            speed : 500
        }
    }


window.show_confirm = (text, position='center', modal=false, ok_callback, ok_callback_params=[], cancel_callback=null, cancel_callback_params=[]) ->
    noty {
        text: text,
        layout: position,
        modal: modal,
        theme: 'custom-notify',
        buttons: [{
            addClass: 'btn btn-primary btn-sm',
            text: 'Ok',
            onClick: ($noty) ->
                $noty.close()
                ok_callback.apply(window, ok_callback_params)
        },{
            addClass: 'btn btn-default btn-sm',
            text: 'Cancel',
            onClick: ($noty) ->
                $noty.close()
                if cancel_callback
                    if cancel_callback_params?.length
                        cancel_callback.apply(window, cancel_callback_params)
                    else
                        cancel_callback()

           }
        ]
    }

window.coords_helper = (coord)->
    tmp = coord[1...-1].replace(' ','').split(',')
    return (parseFloat(x) for x in tmp)