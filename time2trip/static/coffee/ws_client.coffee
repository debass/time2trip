class window.WSClient
    constructor:(@host, @route_id, @token, @reconnected_clb = null) ->
        # @initial_deferred = Q.defer()
        @url = "ws://#{@host}/?route_id=#{@route_id}&token=#{@token}"

        @reconnecting_interval = 1000

        @request_id = 1
        @actions = {}
        @promises = {}
        @pending_requests = []
        @reconnecting = false

        @create_connection()
        # return @initial_deferred.promise

    on_open_clb : =>
        return 

    create_connection: =>
#        settings = {'timeoutInterval': 1000, 'reconnectInterval': 3000, 'reconnectDecay': 1.0, 'debug': true}
#        @connection = new ReconnectingWebSocket( @url, null, settings )
        @connection = new WebSocket(@url)
        @connection.onopen = @on_open
        @connection.onerror = @error_handler
        @connection.onmessage = @receive_handler
        @connection.onclose = @on_close

        window.onbeforeunload = =>
            @connection.close()

    on_open: (evt) =>
        console.log 'Connection established'
        # @receive_first_msg = false

        if @reconnecting
            # @reconnecting = false
            if @reconnected_clb
                @reconnected_clb('reconnected!!!')

        if @pending_requests.length
            for r in @pending_requests
                console.log "[LOG] [WS_CLIENT] Handle pending requests", r
                @pending_requests.pop(r)
                @connection.send(JSON.stringify(r))

    on_close: (evt) =>
        console.log 'Connection was closed', evt
        # if evt.wasClean

        if @reconnected_clb and not @reconnecting
            @reconnected_clb('disconnected')
        @reconnecting = true
        setTimeout(
            () =>
                console.log 'Reconnecting...'
                @create_connection()
            , @reconnecting_interval)

    error_handler: (err) ->
        console.log 'WebSocket error!', err

    receive_handler: (evt) =>
        console.log '[LOG] [WS CLIENT] receive', evt
        data = JSON.parse(evt.data)

        # if not @receive_first_msg
        #     @initial_deferred.resolve({data: data, ws: @})

        if data.request_id
            if @promises[data.request_id]?
                @promises[data.request_id].resolve(data)

        if data.action
            if @actions[data.action]?
                for clb in @actions[data.action]
                    clb(data)


    request: (method, data) =>

        deferred = Q.defer()
        console.log '[LOG] [WS_CLIENT] request', method, data

        new_request_id = @request_id += 1

        request_data = {'method': method, 'data': data, 'request_id': new_request_id}
        @promises[new_request_id] = deferred

        if @connection.readyState != WebSocket.OPEN
            @pending_requests.push(request_data)

        else
            @connection.send(JSON.stringify(request_data))

        return deferred.promise

    register_action: (name, clb) =>
        console.log "Register action for #{name}"
        if not @actions[name]?
            @actions[name] = []

        @actions[name].push(clb)
