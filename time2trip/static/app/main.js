import Vue from 'vue'
import VeeValidate from 'vee-validate'
import VueI18n from 'vue-i18n'

import locales from './locale'
import store from './store'
import router from './routes'

import App from './components/app.vue'

import AppLayout from './components/AppLayout.vue'
import Block from './components/shared/Block.vue'
import vSelect from 'vue-select'

Vue.component('app-page-layout', AppLayout)
Vue.component('block', Block)
Vue.component('v-select', vSelect)

Q.onerror = (err) => console.log(err)



// TODO оформить как-то получше все эти промисы
window['map_loaded_defer'] = Q.defer()
window['map_loaded_promise'] = window['map_loaded_defer'].promise

window['map_ready_defer'] = Q.defer()
window['map_ready_promise'] = window['map_ready_defer'].promise

window['map_load_complete'] = function () {
    window.map_loaded_defer.resolve()
}

window['route_loaded_defer'] = Q.defer()
window['route_loaded_promise'] = window['route_loaded_defer'].promise


// 

//валидировать наличе объекта (поинт, сабпоинт, заметка) в этом хуке, если нет, то редиректить на ошибку
router.beforeEach((to, from, next) => {
    window.route_loaded_promise.then(
        () => {
            //надо как-то упростить эту хуйню
            if ('point_id' in to.params){
                let point_id = parseInt(to.params.point_id)
                if (store.state.route.points.filter((p) => p.id == point_id).length){
                    store.commit('choose_point', parseInt(to.params.point_id))
                    next()
                }
                else{
                    next('/no_point')
                }
            }
            else
                next()
        }
    ).fail((e)=> console.log(e))
})

Vue.use(VueI18n)

Vue.config.lang = 'en'
Vue.config.fallbackLang = 'en'

Object.keys(locales).forEach(function (lang) {
  Vue.locale(lang, locales[lang])
})

Vue.use(VeeValidate)


const app = new Vue({
    router,
    store,
    el: '#app',
    render: h => h(App)
})

// const map_app = new Vue({
//     store,
//     el: '#map',
//     render: h = h()
// })

exports.app = app