import Vue from 'vue'
import Vuex from 'vuex'
import _ from 'lodash'

import {make_marker, make_path} from '../helpers.js'

Vue.use(Vuex)
console.log('store')

export default new Vuex.Store({
    state: {
        route: {},
        route_path: null,
        route_loaded: false,
        screen_spinner_active: false,
        app_spinner_active: false,
        refbooks: {},
        user: {},
        current_point: {},
        current_subpoint: {},
        venues: {},
        app_error: {
            error: '',
            description: ''
        }
    },

    mutations: {

        set (state, data) {
            _.assignIn(state, data)
        },

        add_venues (state, data) {
            state.venues[data.point_id] = data.venues
        },

        edit_route (state, route_obj) {
            _.assignIn(state.route, route_obj)
        },

        choose_point (state, point_id) {
            let point = state.route.points.filter( (p) => p.id == point_id)
            if (point)
                state.current_point = point[0]
        },

        add_point (state, point_data) {
            state.route.points.push(point_data)
            // state.markers[point_data.id] = {
            //     marker: make_marker(point_data, window.map), 
            //     subpoints:{}
            // }
        },

        delete_point (state, point_id, external = false){
            let point = state.route.points.filter( (p) => p.id == point_id)
            if (point)
                point = point[0]
                if (point.id == state.current_point.id)
                    state.current_point = {}
                point.marker.setMap(null)
                state.route.points.splice(state.route.points.indexOf(point), 1)
                // delete state.markers[point_id]
        },


    },

    actions: {
        choose_point (context, point_id){
            context.commit('choose_point', point_id)
        },

        edit_route (context, route_data) {
            let d = Q.defer()
            window.ws.request('edit_route', route_data).then(
                (response) => {
                    if (response.success)
                        context.commit('edit_route', response.result.route)
                    d.resolve(response.result.route)
                }
            ).fail(
                (err) =>
                    console.log(err)
            )
            return d.promise
        },

        add_point (context, point_data) {
            let d = Q.defer()
            window.ws.request('add_point', point_data).then(
                (response) => {
                    if (response.success) {
                        let point = response.result.point
                        point.marker = make_marker(point, window.map)
                        context.commit('add_point', point)
                        d.resolve(response.result.point.id)
                    }
                }
            
            ).fail(
                (err) =>
                    console.log(err)
            )
            return d.promise
        },

        delete_point (context, point_id) {
            let d = Q.defer()
            window.ws.request('delete_point', {'id': point_id}).then(
                (response) => {
                    if (response.success)
                        context.commit('delete_point', response.result.point.id)
                        d.resolve(response.result.point)
                }
            ).fail(
                (err) =>
                    console.log(err)
            )
            return d.promise
        }
    },

    getters: {
        app_error (state) {
            return state.app_error
        },
        route_loaded (state){
            return state.route_loaded
        },
        locale (state) {
            return state.user.locale
        },
        current_point (state) {
            return state.current_point
        },
        points (state) {
            return state.route.points
        },
        markers (state){
            return state.markers
        },
        route_for_edit (state) {
            return {
                'title': state.route.title,
                'description': state.route.description
            }
        },
        foursquare_venues (state) {
            return state.venues
        }
    }
})