# -*- coding:utf-8 -*-

import json
import os, random, string
import base64
import datetime
from uuid import uuid4
import logging
from logging.handlers import SMTPHandler

from webob.multidict import MultiDict


from pyramid.security import authenticated_userid, forget, Everyone

from time2trip.db import db_session, models
from time2trip import constants

def get_identifiers(user_id, request):
    user = db_session.query(models.User).filter(models.User.id == user_id).first()
    if not user:
        return Everyone

    if user.is_admin:
        return ['group:admin']
    else:
        return ['group:user']

def get_user(request):
    user_id = authenticated_userid(request)
    if not user_id:
        request.response.headerlist.extend(forget(request))
        return None

    user = db_session.query(models.User).filter_by(id=user_id).first()

    return user


def datetime_json_encoder(obj, request):
    #TODO: add different datetime string for en and ru locales
    return obj.isoformat() if isinstance(obj, (datetime.datetime, datetime.date)) else None


def pass_gen(length=10):
    chars = string.ascii_letters + string.digits + '!@#$%^&*()'
    random.seed = (os.urandom(1024))
    return ''.join(random.choice(chars) for i in range(length))

def generate_uuid():
    return str(uuid4()).replace('-', '')

def locale_negotiator (request):
    locale_name = getattr(request, constants.LOCALE_COOKIE_NAME, None)
    if locale_name is None:
        locale_name = request.params.get(constants.LOCALE_COOKIE_NAME)
        if locale_name is None:
            locale_name = request.cookies.get(constants.LOCALE_COOKIE_NAME)
            if locale_name is None:
                if request.authenticated_userid:
                    locale_name = request.user.options.get('language', None)
                    return locale_name
                locale_name = request.accept_language.best_match(
                    constants.LOCALES, request.registry.settings.default_locale_name)
                if not request.accept_language:
                    locale_name = request.registry.settings.default_locale_name
    return locale_name