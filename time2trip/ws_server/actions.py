# -*- coding:utf-8 -*-

import time
import functools
from copy import copy
from urllib import quote
from logging import getLogger

import grequests

from time2trip import forms, constants as const
from time2trip.db import models, db_session

log = getLogger('time2trip')

def check_schema(schema=None):
    def decorator(callback):

        @functools.wraps(callback)
        def wrapper(*args, **kwargs):
            start_time = time.time()
            app = kwargs['app']
            data = kwargs['data']

            if schema:
                if app.user:
                    data['user_id'] = app.user.id
                form = schema(data = data)

                if not form.validate():
                    return dict(success=False, err_msg=form.errors)

            result = callback(*args, **kwargs)

            print "Profiling >>> {} rutime={}".format(callback.__name__, time.time() - start_time)

            return result

        return wrapper

    return decorator


class Actions(object):

    @staticmethod
    def response(result):
        return {'success': True, 'result': result}

    @staticmethod
    def error_response(err_msg):
        return {'success': False, 'err_msg': err_msg}

    @staticmethod
    def request(urls):
        if not isinstance(urls, list):
            urls = [urls]

        one_url = False if len(urls) > 1 else True

        results = grequests.map([grequests.get(url) for url in urls])
        print results
        return results

        if one_url:
            return results[0]
        else:
            return results


    @staticmethod
    def get_point_names(google_place_id):
        cache_item = models.PointNameCache.get(google_place_id)
        if cache_item:
            return cache_item

        result = {'title':{}}
        rs = [
            grequests.get(const.GOOGLE_PLACE_API.format(google_place_id, 'AIzaSyCqWnzZvrNJOoCjzZDMCX-br5__rQecxP4', locale), verify=False)
            for locale in const.LOCALES
        ]

        request_results = grequests.map(rs)

        for locale, _result in zip(const.LOCALES, request_results):
            if _result:
                json_result = _result.json()
                if json_result['status'] == 'OK':
                    result['title'].update({locale: json_result['result']['name']})
                else:
                    log.error('Error while request to Google Api. status={}'.format(json_result['status']))

        models.PointNameCache.set(google_place_id, result)

        return result

    @staticmethod
    def make_url (base_url, params):
        if not base_url.endswith('?'):
            url = base_url + '?'
        else:
            url = base_url

        url += '&'.join(["{}={}".format(k, quote(str(v))) for k,v in params.items()])
        return url

    @classmethod
    @check_schema(schema = forms.Authentication)
    def authentication (cls, data, app):
        user, route = db_session.query(
            models.User,
            models.Route
        ).join(
            models.Route
        ).filter(
            models.User.ws_token == data['token'],
            models.Route.id == data['route_id']
        ).first()

        if user:
            return cls.response({'user': user, 'route': route})
        else:
            return cls.error_response({'user': None, 'route': None})

    @classmethod
    @check_schema(schema = forms.Route)
    def get_route(cls, data, app):
        route = db_session.query(models.Route).filter_by(id=data['route_id']).first()
        if not route:
            return cls.error_response('route_not_found')

        result = {'route': route.to_dict(), 'user': app.user.to_dict()}

        return cls.response(result)

    @classmethod
    @check_schema()
    def edit_route(cls, data, app):
        route = db_session.query(models.Route).filter_by(id=data['route_id']).first()
        route.fromDict(data)

        db_session.add(route)
        db_session.flush()
        db_session.refresh(route)

        return cls.response({'route': {
            'title': route.title,
            'description': route.description
        }})

    @classmethod
    @check_schema(schema = forms.NewPoint)
    def add_point(cls, data, app):
        new_point = models.Point()
        new_point.fromDict(data)

        for x in data['address_components']:
            if "country" in x["types"]:
                country_name = x["short_name"]
                new_point.country = country_name
            else:
                new_point.country = '_none'
        localized_names = cls.get_point_names(data['gmaps_placeid'])

        if localized_names:
            new_point.locale = localized_names
        else:
            new_point.locale = {"title": {app.user.locale: data['title']}}

        new_point.creator = app.user.id
        db_session.add(new_point)
        db_session.flush()
        db_session.refresh(new_point)

        route = db_session.query(models.Route).filter_by(id = new_point.route_id).first()
        new_order = copy(route.order)
        new_order.append(new_point.id)
        route.order = new_order

        db_session.add(route)

        return cls.response({"point": new_point.to_dict()})

    @classmethod
    @check_schema(schema = forms.DeletePoint)
    def delete_point(cls, data, app):
        point = db_session.query(models.Point).filter(models.Point.id == data['id'], models.Point.route_id == data['route_id']).first()
        if not point:
            return cls.error_response('point_not_found')

        point.delete()

        return cls.response({"point": point.to_dict()})

    @classmethod
    def _sort_venue_tips(cls, tips, app):
        def sorting_function(k):
            if k is None or not k:
                k = 'z'
            return sort_order.index(k) if k in sort_order else ord(k[0])

        if app.user.locale == 'en':
            sort_order = ['en']
        elif app.user.locale == 'ru':
            sort_order = ['ru', 'en']

        return sorted(tips, key=lambda k: sorting_function(k.get('lang','z')))


    @classmethod
    def parse_venue(cls, venue, app, use_geocoder = False):
        PHOTO_SIZE = "width800"
        venue_dict = dict(
            id=venue['id'],
            name=venue['name'],
            description=venue.get('description'),
            url=venue.get('url'),
            foursquare_url=venue['shortUrl'],
            rating=venue.get('rating'),

            lat=venue['location']['lat'],
            lng=venue['location']['lng'],

            address=venue['location'].get('address'),

            phone=venue['contact'].get('phone'),
            twitter=venue['contact'].get('twitter')
        )

        if use_geocoder:
            pass

        venue_dict['category'] = venue['categories'][0]['name']

        for attr in venue['attributes']['groups']:
            if attr['type'] == 'price':
                venue_dict['price'] = attr['summary']
            elif attr['type'] == 'payments':
                venue_dict['credit_card'] = attr['items'][0]['displayValue'].lower().startswith('y')
            elif attr['type'] == 'outdoorSeating':
                venue_dict['outdoor_seating'] = attr['items'][0]['displayValue'].lower().startswith('y')
            elif attr['type'] == 'wifi':
                venue_dict['wifi'] = attr['items'][0]['displayValue'].lower().startswith('f')

        venue_photos = []
        try:
            for photo in venue['photos']['groups'][0]['items']:
                venue_photos.append('{prefix}{photo_size}{suffix}'.format(
                    prefix=photo['prefix'],
                    photo_size=PHOTO_SIZE,
                    suffix=photo['suffix']
                ))
        except:
            pass

        venue_dict['photos'] = venue_photos

        tmp = []
        for tip in venue['tips']["groups"][0]["items"]:
            tmp.append({'id': tip['id'], 'lang': tip.get('lang', None), 'text': tip['text'], 'type': tip['type'], 'ctime': tip['createdAt']})

        venue_dict['tips'] = cls._sort_venue_tips(tmp, app)

        return venue_dict


    @classmethod
    def get_venues(cls, venue_ids, app):
        if not isinstance(venue_ids, list):
            venue_ids = [venue_ids]

        one_venue = False if len(venue_ids) > 1 else True

        client_id = 'ENPSPIN3SSJ5ZVI5YEYQRA4I4TWGGVF0BQC5CB2ZXRAVICL4'
        client_secret = 'ISW4AKIFMMYWL13FS1QYTTWB30VYKDNQDUZUPAKJQTBGTHAQ'

        urls = [cls.make_url("https://api.foursquare.com/v2/venues/{}".format(v_id), {'locale': app.user.locale,'client_id': client_id,'client_secret': client_secret,'v': '20160702'}) for v_id in venue_ids]

        result = [cls.parse_venue(v.json()['response']['venue'], app) for v in cls.request(urls)]

        if one_venue:
            return result[0]
        else:
            return result

    @classmethod
    @check_schema()
    def get_foursquare_venue (cls, data, app):
        venue_id = data['venue_id']
        result = cls.get_venues(venue_id, app)

        return cls.response({'venue': result})


    @classmethod
    @check_schema()
    def get_foursquare_venues(cls, data, app):
        point = db_session.query(models.Point).filter_by(id = data.get('point')).first()
        # exists_foursquare_venues = [subpoint.options.get('foursquare_id') for subpoint in db_session.query(models.Subpoint).filter_by(source='foursquare', point = point.id)]

        client_id = 'ENPSPIN3SSJ5ZVI5YEYQRA4I4TWGGVF0BQC5CB2ZXRAVICL4'
        client_secret = 'ISW4AKIFMMYWL13FS1QYTTWB30VYKDNQDUZUPAKJQTBGTHAQ'

        point_name = point.localize_attr('title', app.user.locale).encode('utf-8')

        cache_key = '{}_{}'.format(data['category'].lower(), point_name)

        if data['category'].lower() == 'top':
            cache_item = models.FoursquareCache.get(cache_key)
            if not cache_item:
                url = cls.make_url(
                    'https://api.foursquare.com/v2/venues/search',
                    {
                        'near': point_name,
                        'categoryId': ",".join(const.FOURSQUARE_CATEGORY['top']),
                        'intent': 'browse',
                        'locale': app.user.locale,
                        'limit': 50,
                        'client_id': client_id,
                        'client_secret': client_secret,
                        'v': '20160702'
                    })
                search_response = cls.request(url)[0]
                venue_ids = [v['id'] for v in search_response.json()['response']['venues']]
                result = cls.get_venues(venue_ids, app)



        return cls.response({'venues': result})

