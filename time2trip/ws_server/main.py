# -*- coding:utf-8 -*-

import psycogreen.gevent
import gevent
import gevent.monkey

gevent.monkey.patch_all()
psycogreen.gevent.patch_psycopg()

import logging
import os
import json
import argparse
import functools 
from ConfigParser import ConfigParser
from collections import defaultdict, OrderedDict

from geventwebsocket import Resource


from pyramid.paster import setup_logging
import sqlalchemy

from time2trip.db import init_db

from base import WSAplication, WSServer

log = logging.getLogger('time2trip')


def run (config):
    port = config.getint('main', 'port')
    engine = sqlalchemy.engine_from_config({'sqlalchemy.url': config.get('database', 'url')})
    engine.pool._use_threadlocal = True
    init_db(engine)
    ws_server = WSServer(
        ('0.0.0.0', port),
        Resource(OrderedDict([
            ('/', WSAplication),
        ])),
        debug = False
    )
    try:
        log.info('WS Server started! Port: {}'.format(port))
        ws_server.serve_forever()
    except KeyboardInterrupt:
        log.info("Keyboard interruped")

    else:
        log.info("[!] stopped due to error")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = 'time2trip websocket server')
    parser.add_argument('config', type = str, help = "configuration file")
    args = parser.parse_args()

    config = ConfigParser()
    config.readfp(open(args.config, 'r'))

    setup_logging(args.config)

    run(config)