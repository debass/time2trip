# -*- coding:utf-8 -*-
import transaction
from logging import getLogger
from collections import defaultdict

import json
import gevent
from geventwebsocket import WebSocketServer, WebSocketApplication
from gevent.lock import Semaphore


from actions import Actions

log = getLogger('time2trip')

def error_response (err_msg, request_id = None):
    result = {'success': True, 'result': err_msg}
    if request_id:
        result['request_id'] = request_id
    return json.dumps(result)

def response (result, request_id = None):
    result = {'success': True, 'result': result}
    if request_id:
        result['request_id'] = request_id
    return json.dumps(result)

class WSServer (WebSocketServer):

    def __init__ (self, *args, **kwargs):
        self.clients_by_route = defaultdict(list)

        super(WSServer, self).__init__(*args, **kwargs)


class WSAplication (WebSocketApplication):
    def on_open (self):
        self.semaphore = Semaphore()
        self.auth = False
        self.user = None
        self.request_id = None
        log.debug('new connections!: {}'.format(self))

        query = self.get_query()

        result = Actions.authentication(
            data = {'route_id': query.get('route_id'), 'token': query.get('token')},
            app = self
        )

        log.debug('Authentication result: {}'.format(result))

        if result['success']:
            self.auth = True,
            self.user = result['result']['user']
            self.route_id = int(result['result']['route'].id)
            self.add_me()
            self.ws_send(response({'current_user': self.user.to_dict()}))
        else:
            self.ws_send(error_response('unauthorized'))

        # transaction.commit()

    def handle_request (self, data):
        method_name = data['method']
        data = data['data']

        method = getattr(Actions, method_name, None)

        if not method:
            self.ws_send(error_response('unknown_method'))

        data['route_id'] = self.route_id
        # try:
        result = method(data = data, app = self)
        # except Exception:
            # self.ws_send(error_response('server_error'))
        # else:

        transaction.commit()

        if result['success']:
            self.ws_send(response(result['result'], request_id = self.request_id))
        else:
            self.ws_send(error_response(result['err_msg'], request_id = self.request_id))


    def on_message (self, message):
        log.debug('message: {}'.format(message))

        if message is None:
            return

        if not self.auth:
            self.ws_send(error_response('unauth'))
            return

        try:
            data = json.loads(message)
            if not isinstance(data, dict) or 'data' not in data.keys() or 'method' not in data.keys():
                self.ws_send(error_response('invalid_message_format'))
                return
        except ValueError:
            self.ws_send(error_response('bad_request_format'))

        else:
            self.request_id = data.get('request_id')
            gevent.spawn(self.handle_request, data)


    def on_close (self, reason):
        self.forget_me()

    def ws_send (self, data):
        with self.semaphore:
            self.ws.send(data)

    def get_query (self):
        query_string = self.ws.environ['QUERY_STRING']
        return dict(x.split('=') for x in query_string.split('&'))

    def broadcast (self, data):
        for ws_app in self.route_clients():
            if not isinstance(ws_app, self):
                ws_app.ws_send(data)

    def add_me (self):
        if not self.route_id:
            return False
        self.ws.handler.server.clients_by_route[self.route_id].append(self)

    def route_clients (self):
        if not self.route_id:
            return []

        return self.ws.handler.server.clients_by_route[self.route_id]

    def forget_me (self):
        # if self in self.ws.handler.server.clients_by_route[self.route_id]:
        self.ws.handler.server.clients_by_route[self.route_id].remove(self)
