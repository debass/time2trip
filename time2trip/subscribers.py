# -*- coding:utf-8 -*-

from pyramid.i18n import TranslationStringFactory
from pyramid.events import subscriber, BeforeRender, NewRequest, NewResponse


tsf = TranslationStringFactory('time2trip')

@subscriber(BeforeRender)
def add_renderer_globals(event):
    request = event['request']
    event['_'] = request.translate
    event['_locale_name'] = request.locale_name
    event['localizer'] = request.localizer


@subscriber(NewRequest)
def add_localizer(event):
    request = event.request
    localizer = request.localizer

    def auto_translate(*args, **kwargs):
        return localizer.translate(tsf(*args, **kwargs))

    request.translate = auto_translate
