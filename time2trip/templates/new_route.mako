<%inherit file="time2trip:templates/base.mako"/>
<%namespace name="defs" file="time2trip:templates/defs.mako"/>

<%block name="title">${_("Edit route")} ${route.title}</%block>

<%block name="styles">
    <link href="${request.static_url('time2trip:static/css/styles.css')}" rel="stylesheet">
    <link href="${request.static_url('time2trip:static/plugins/font-awesome/font-awesome.min.css')}" rel="stylesheet">
    ## <link href="${request.static_url('time2trip:static/css/external/summernote.css')}" rel="stylesheet">

</%block>


<%block name="scripts">
    
    <script src="${request.static_url('time2trip:static/plugins/q.min.js')}"></script>
    <script src="${request.static_url('time2trip:static/js/ws_client.js')}"></script>
    <script type="text/javascript">
        window.token = "${request.user.ws_token}"
        window.route_id = ${route.id}
    </script>
</%block>


<div id="app" style="height:100%">

</div>

<script src="${request.static_url('time2trip:static/dist/build.js')}"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=${request.registry.settings.get('google.maps.key')}&language=${request.locale_name}&callback=map_load_complete"></script>
