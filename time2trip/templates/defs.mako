<%def name="field_with_errors(field, **kwargs)">
  % if field.errors:
    ${field(class_='has-error ' + (" required " if "required" in field.flags else "") + kwargs.pop('class_', ''), **kwargs)}
    <ul style="${kwargs.pop('alert_style','')} list-style-type: none; margin-top: 5px; color: red; padding-left: 0px; text-align: center;">
      %for error in field.errors:
        <li>${error}</li>
      %endfor
    </ul>
  % else:
    % if "required" in field.flags:
      ${field(class_ = kwargs.pop('class_', "") + " required ", **kwargs)}
    % else:
      ${field(class_ = kwargs.pop('class_', ""), **kwargs)}
    % endif
  % endif
</%def>

<%def name="route(route, type='route')">
  <div class="panel panel-primary route">
    <div class="panel-heading">
      <div class="route-header" data-route_id="${route.id}">
        <a href='#${type}-${route.id}' data-parent=${"#routes" if type == 'route' else "#shared_routes"} data-toggle="collapse" class="route-info-link" style="border-right: 1px solid #CCC; margin-left: 5px; padding-right: 10px;"> ${route.title}</a>

        ## %if route.date_start and route.date_finish:
        ##   <span style="color:#cccccc;  border-right: 1px solid #CCC; margin-left: 10px; padding-right: 10px; margin-right: 10px" class="hidden-sm hidden-xs">
        ##     ${route.date_start.strftime('%d/%m/%Y')} - ${route.date_finish.strftime('%d/%m/%Y')}
        ##   </span>
        ## %endif

        <a href="${request.route_url('route', route_id=route.id)}" class="btn btn-default btn-xs">
          <i class="fa fa-pencil"></i><span class="hidden-xs"> ${_("Edit")}</span>
        </a>

        %if route.points:
          <a href="#" class="btn btn-default btn-xs"><i class="fa fa-bars"></i><span class="hidden-xs"> ${_("Summary")}</span>
          </a>
        %endif

        <div class="pull-right">
          <div class='btn-group-xs btn-group'>
            <a class="btn btn-default" href="${request.route_url('route.delete', route_id = route.id)}"><span class='glyphicon glyphicon-remove'></span></a>
          </div>
        </div>
      </div>
      <div class="delete_route_confirm"
         style="display: none; text-align: center; text-align: center; background: white; margin-left: -15px; margin-right: -15px; margin-top: -10px; height: 40px; padding-top: 5px; margin-bottom: -10px;"
         data-route_id="${route.id}">
        <a class="btn btn-danger btn-sm" data-route_id='${route.id}' href="#"> ${_('Yes')}</a>
        <span class="btn btn-default btn-sm" data-route_id='${route.id}'
            onclick="want_to_delete_route(this);"> ${_('No')}</span>
      </div>
    </div>
    <div class='collapse panel-collapse' id='${type}-${route.id}'>
      <ul class="list-group">

        % if route.description:
          <li class="list-group-item">${route.description}</li>
        % endif

        % if route.points:
          <li class="list-group-item">
            ${route.points[0].localize_attr('name', request.locale_name)}
            % for p in route.points[1:]:
              - ${p.localize_attr('name', request.locale_name)}
            % endfor
          </li>
        % endif

        
        <li class="list-group-item">
          <a href="#" class="btn btn-default btn-xs"><i class="fa fa-file-pdf-o"></i> ${_("Export to PDF")}</a>
          <a href="#" class="btn btn-default btn-xs"><i class="fa fa-file-code-o"></i> ${_("Download KML file")} </a>
        </li>
      </ul>
    </div>
  </div>

</%def>