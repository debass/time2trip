# -*- coding:utf-8 -*-

from sqlalchemy.ext.declarative import declarative_base, DeferredReflection
from sqlalchemy.orm import scoped_session, sessionmaker
from zope.sqlalchemy import ZopeTransactionExtension

db_session = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))
db_session_be = scoped_session(sessionmaker())
Base = declarative_base(cls = DeferredReflection)
# meta = MetaData()

def init_db(engine):
    Base.metadata.bind = engine
    db_session.configure(bind=engine)

    import time2trip.db.models

    Base.metadata.reflect()
    Base.prepare(engine)