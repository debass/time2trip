CREATE TABLE users__routes (
    user_id BIGINT NOT NULL REFERENCES users(id) ON DELETE CASCADE,
    route_id BIGINT NOT NULL REFERENCES routes(id) ON DELETE CASCADE,
    permission text NOT NULL,
    ctime TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT timezone('utc', now()),
    PRIMARY KEY (user_id, route_id)
);