CREATE TABLE point_name_chache (
    gmap_id VARCHAR(255) NOT NULL PRIMARY KEY,
    value JSON NOT NULL,
    expire_at TIMESTAMP WITH TIME ZONE NOT NULL
);