
CREATE TABLE users (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    email VARCHAR( 255 ) NOT NULL DEFAULT '',
    is_email_confirmed BOOL NOT NULL DEFAULT false,
    "password" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    social_networks JSONB NOT NULL DEFAULT '{}'::jsonb,
    "options" JSONB NOT NULL DEFAULT '{}'::jsonb,
    is_active BOOL NOT NULL DEFAULT false,
    email_confirmation_token text NOT NULL DEFAULT '',
    ws_token VARCHAR(255) NOT NULL DEFAULT '',
    is_admin BOOL NOT NULL DEFAULT false,
    ctime TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT timezone('utc', now())
);


CREATE TABLE routes (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    user_id BIGINT NOT NULL REFERENCES users(id) ON DELETE CASCADE,
    title VARCHAR (255) NOT NULL,
    description TEXT,
    "order" INTEGER[] NOT NULL DEFAULT ARRAY[]::INTEGER[],
    ctime TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT timezone('utc', now())
);


CREATE TABLE points (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    route_id bigint not null REFERENCES routes(id) ON DELETE CASCADE,
    lat FLOAT NOT NULL,
    lng FLOAT NOT NULL,
    locale JSONB NOT NULL DEFAULT '{}'::jsonb,
    country VARCHAR (255),
    bounds JSONB NOT NULL DEFAULT '{}'::jsonb,
    gmaps_placeid VARCHAR (255),
    creator_id BIGINT,
    ctime TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT timezone('utc', now())
);

CREATE TABLE subpoints_types (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    key VARCHAR(255) NOT NULL,
    name_ru VARCHAR(255) NOT NULL,
    name_en VARCHAR(255) NOT NULL,
    description_en TEXT NOT NULL DEFAULT '',
    description_ru TEXT NOT NULL DEFAULT '',
    "order" SMALLINT NULL,
    is_default BOOL NOT NULL DEFAULT FALSE
);


INSERT INTO subpoints_types(key, name_ru, name_en, description_en, description_ru, "order", is_default)
VALUES
  ('SIGHT', 'Достопримечательность', 'Sight', '', '', '1', TRUE),
  ('ACCOMODATION', 'Жилье', 'Accomodation', '', '', '2', FALSE),
  ('PHOTO_POINT', 'Фото', 'Photo point', '', '', '3', FALSE),
  ('TRANSPORT_POINT', 'Транспортный узел', 'Transport point', '', '', '4', FALSE),
  ('EVENT', 'Событие', 'Event', '','','5',FALSE),
  ('OTHER', 'Прочее', 'Other', '', '', '6', FALSE);


CREATE TABLE subpoints (
  id bigserial not null PRIMARY KEY,
  type bigint NULL REFERENCES subpoints_types(id),
  point_id bigint not null REFERENCES points(id) ON DELETE CASCADE,
  source varchar(255) not null,
  title varchar(255) not null,
  address text,
  note text NOT NULL DEFAULT '',
  lat float not null,
  lng float not null,
  bounds json NOT NULL DEFAULT '{}'::json,
  options json NOT NULL DEFAULT '{}'::json,
  creator_id bigint NOT NULL REFERENCES users(id),
  ctime timestamp with time zone not null DEFAULT now()
);