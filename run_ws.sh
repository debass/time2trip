#!/usr/bin/env bash
set -e

if [ "$#" -lt 1 ]
then
    echo "usage error: provide config as parameter"
    exit 1
fi

patterns="*.py"

if [[ "$OSTYPE" == "linux-gnu" ]]; then
   watchmedo auto-restart \
    --patterns "*.py" \
    -d ./time2trip/ws_server \
    -d ./time2trip/db \
    bash -- -c "python time2trip/ws_server/main.py \"$@\""

elif [[ "$OSTYPE" == "darwin"* ]]; then
    watchmedo auto-restart \
    --patterns "*.py" \
    -d ./time2trip/ws_server \
    -d ./time2trip/db \
    "python time2trip/ws_server/main.py \"$@\""
fi


